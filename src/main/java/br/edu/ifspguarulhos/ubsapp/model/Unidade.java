package br.edu.ifspguarulhos.ubsapp.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Unidade")
public class Unidade {
	
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Id
	private Integer idUnidade;
	
	@Column(name = "nome")
	private String nome;
	
	@Column(name = "endereco")
	private String endereco;
	
	@Column(name = "idDiretor")
	private Integer idDiretor;
		
	public Integer getIdUnidade() {
		return idUnidade;
	}
	
	public void setIdUnidade(Integer idUnidade) {
		this.idUnidade = idUnidade;
	}
	
	public String getNome() {
		return nome;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public String getEndereco() {
		return endereco;
	}
	
	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}
	
	public Integer getIdDiretor() {
		return idDiretor;
	}
	
	public void setIdDiretor(Integer idDiretor) {
		this.idDiretor = idDiretor;
	}
}
