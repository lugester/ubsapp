package br.edu.ifspguarulhos.ubsapp.model;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

public class Escala {
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Id
	private Integer idEscala;

	@Column(name = "nome")
	private Integer idFuncionario;

	@Column(name = "nome")
	private Date segunda;

	@Column(name = "nome")
	private Date terca;

	@Column(name = "nome")
	private Date quarta;

	@Column(name = "nome")
	private Date quinta;

	@Column(name = "nome")
	private Date sexta;

	public Integer getIdEscala() {
		return idEscala;
	}

	public void setCodEscala(Integer idEscala) {
		this.idEscala = idEscala;
	}

	public Integer getIdFuncionario() {
		return idFuncionario;
	}

	public void setIdFuncionario(Integer idFuncionario) {
		this.idFuncionario = idFuncionario;
	}

	public Date getSegunda() {
		return segunda;
	}

	public void setSegunda(Date segunda) {
		this.segunda = segunda;
	}

	public Date getTerca() {
		return terca;
	}

	public void setTerca(Date terca) {
		this.terca = terca;
	}

	public Date getQuarta() {
		return quarta;
	}

	public void setQuarta(Date quarta) {
		this.quarta = quarta;
	}

	public Date getQuinta() {
		return quinta;
	}

	public void setQuinta(Date quinta) {
		this.quinta = quinta;
	}

	public Date getSexta() {
		return sexta;
	}

	public void setSexta(Date sexta) {
		this.sexta = sexta;
	}
}
