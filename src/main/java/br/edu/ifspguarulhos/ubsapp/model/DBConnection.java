package br.edu.ifspguarulhos.ubsapp.model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class DBConnection {
	private Connection conn;
	private String driver;
	private String url;
	private String username;
	private String password;
	
	
	public DBConnection() {
		Properties prop = Propriedades.getPropriedades();
		
		this.setDriver(prop.getProperty("spring.datasource.driver-class-name"));
		this.setUrl(prop.getProperty("spring.datasource.url"));
		this.setUsername(prop.getProperty("spring.datasource.username"));
		this.setPassword(prop.getProperty("spring.datasource.password"));
		this.setConn(this.getConnection());
	}
	
	
	public void forNameDriver() {
		try {
			Class.forName(this.driver);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public Connection getConnection() {
		Connection connect = null;
		if (this.getConn() == null) {
			this.forNameDriver();
			try {
				connect = DriverManager.getConnection(this.url, this.username, this.password);
				System.out.println("Conectado!!!");
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return connect;
	}
	
	public void closeConnection() {
		try {
			if(!this.getConn().isClosed()) {
				this.getConn().close();
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
	}
	
	
	public Connection getConn() {
		return conn;
	}
	public void setConn(Connection conn) {
		this.conn = conn;
	}
	public String getDriver() {
		return driver;
	}
	public void setDriver(String driver) {
		this.driver = driver;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	
	
	
}
