package br.edu.ifspguarulhos.ubsapp.model;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

public class Equipamento {
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Id
	private Integer idEquipamento;

	@Column(name = "nome")
	private String nome;

	@Column(name = "data_recebido")
	private Date data_recebido;

	@Column(name = "validade")
	private Date validade;

	@Column(name = "tipo")
	private String tipo;

	@Column(name = "idFornecedor")
	private Integer idFornecedor;

	@Column(name = "finalidade")
	private String finalidade;

	@Column(name = "idDepartamento")
	private Integer idDepartamento;

	@Column(name = "reutilizavel")
	private Boolean reutilizavel;

	public Integer getIdEquipamento() {
		return idEquipamento;
	}

	public void setIdEquipamento(Integer idEquipamento) {
		this.idEquipamento = idEquipamento;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Date getData_recebido() {
		return data_recebido;
	}

	public void setData_recebido(Date data_recebido) {
		this.data_recebido = data_recebido;
	}

	public Date getValidade() {
		return validade;
	}

	public void setValidade(Date validade) {
		this.validade = validade;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public Integer getIdFornecedor() {
		return idFornecedor;
	}

	public void setIdFornecedor(Integer idFornecedor) {
		this.idFornecedor = idFornecedor;
	}

	public String getFinalidade() {
		return finalidade;
	}

	public void setFinalidade(String finalidade) {
		this.finalidade = finalidade;
	}

	public Integer getIdDepartamento() {
		return idDepartamento;
	}

	public void setIdDepartamento(Integer idDepartamento) {
		this.idDepartamento = idDepartamento;
	}

	public Boolean getReutilizavel() {
		return reutilizavel;
	}

	public void setReutilizavel(Boolean reutilizavel) {
		this.reutilizavel = reutilizavel;
	}
}
