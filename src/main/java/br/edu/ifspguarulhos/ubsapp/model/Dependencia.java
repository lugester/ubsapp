package br.edu.ifspguarulhos.ubsapp.model;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

public class Dependencia {
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Id
	private Integer idDependecia;
	
	@Column(name = "nome")
	private String nome;
	
	@Column(name = "idUnidade")
	private Integer idUnidade;
	
	public Integer getIdDependecia() {
		return idDependecia;
	}
	public void setIdDependecia(Integer idDependecia) {
		this.idDependecia = idDependecia;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public Integer getIdUnidade() {
		return idUnidade;
	}
	public void setIdUnidade(Integer idUnidade) {
		this.idUnidade = idUnidade;
	}
	
}
