package br.edu.ifspguarulhos.ubsapp.model;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

public class Fornecedor {

	@GeneratedValue(strategy = GenerationType.AUTO)
	@Id
	private Integer idFornecedor;

	@Column(name = "nome")
	private String nome;

	public Integer getIdFornecedor() {
		return idFornecedor;
	}

	public void setIdFornecedor(Integer idFornecedor) {
		this.idFornecedor = idFornecedor;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
}
