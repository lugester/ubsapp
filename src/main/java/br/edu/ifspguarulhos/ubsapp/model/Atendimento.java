package br.edu.ifspguarulhos.ubsapp.model;

import java.sql.Date;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;

public class Atendimento {
	@GeneratedValue(strategy = GenerationType.AUTO)

	@Id
	private Integer idAtendimento;

	@JoinColumn(name = "idPaciente")
	private Paciente idPaciente;
	
	@JoinColumn(name = "idFuncionario")
	private Funcionario idFuncionario;
	
	@Column(name = "data_agendamento")
	private Date data_agendamento;
	
	@Column(name = "dataMarcada")
	private Timestamp dataMarcada;
	
	@Column(name = "status_atend")
	private String status_atend;
	
	@Column(name = "observacao")
	private String observacao;

	public Integer getIdAtendimento() {
		return idAtendimento;
	}

	public void setIdAtendimento(Integer id) {
		this.idAtendimento = id;
	}

	public Paciente getIdPaciente() {
		return idPaciente;
	}

	public void setIdPaciente(Paciente idPaciente) {
		this.idPaciente = idPaciente;
	}

	public Funcionario getIdFuncionario() {
		return idFuncionario;
	}

	public void setIdFuncionario(Funcionario idFuncionario) {
		this.idFuncionario = idFuncionario;
	}

	public Date getData_agendamento() {
		return data_agendamento;
	}

	public void setData_agendamento(Date data_agendamento) {
		this.data_agendamento = data_agendamento;
	}

	public Timestamp getDataMarcada() {
		return dataMarcada;
	}

	public void setDataMarcada(Timestamp dataMarcada) {
		this.dataMarcada = dataMarcada;
	}

	public String getStatus_atend() {
		return status_atend;
	}

	public void setStatus_atend(String status_atend) {
		this.status_atend = status_atend;
	}

	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

}