package br.edu.ifspguarulhos.ubsapp.model;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class DBQuery {
	private String tabela;
	private String[] campos;
	private DBConnection conn;

	public DBQuery(String tabela, String[] campos, DBConnection conn) {
		this.setTabela(tabela);
		this.setCampos(campos);
		this.setConn(conn);
	}

	public ResultSet select(String campoWhere, String[] valorWhere) {

		String sql = "SELECT ";
		ResultSet rs = null;

		if (this.conn.getConn() != null) {
			PreparedStatement stmt = null;
			for (int i = 0; i < this.campos.length; i++) {
				sql += this.campos[i];
				sql += (i == this.campos.length - 1) ? " " : ", ";
			}
			sql += "FROM " + this.tabela;
			
			if(campoWhere != null) {
				sql += " WHERE ";
				
				for (int i = 0; i < valorWhere.length; i++) {
					sql += campoWhere + " like '" + valorWhere[i] + "'";
					sql += (i == valorWhere.length - 1) ? "; " : " OR "; 
				}
				
			}
			
			
			try {
				System.out.println(sql);
				stmt = this.conn.getConn().prepareStatement(sql);
				rs = stmt.executeQuery();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

		return rs;
	}

	@SuppressWarnings("unused")
	public Integer update(Object[] novosValores, String campoWhere, Object valorWhere) {
		Integer rows = 0;
		PreparedStatement stmt = null;
		String sql;
		sql = "UPDATE " + this.getTabela() + " SET ";
		for (int i = 0; i < this.campos.length; i++) {
			sql += this.campos[i] + " = '" + novosValores[i] + "' ";
			sql += (i == this.campos.length - 1) ? " " : ", ";

		}
		sql += "WHERE " + campoWhere + " = " + valorWhere + ";";

		System.out.println(sql);

		try {
			stmt = this.conn.getConn().prepareStatement(sql);
			rows = stmt.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return rows;
	}

	public Integer insert(Object[] valoresInsert) {
		Integer rows = 0;
		PreparedStatement stmt = null;
		String sql;
		sql = "INSERT INTO " + this.tabela + "(";

		if (this.conn.getConn() != null) {
			for (int i = 0; i < this.campos.length; i++) {
				sql += this.campos[i];
				sql += (i == this.campos.length - 1) ? ") " : ", ";
			}
			sql += "VALUES (";
			for (int i = 0; i < this.campos.length; i++) {
				sql += "?";
				sql += (i == this.campos.length - 1) ? ");" : ", ";
			}

			try {
				stmt = this.conn.getConn().prepareStatement(sql);
				for (int i = 0; i < valoresInsert.length; i++) {
					stmt.setObject(i + 1, valoresInsert[i]);
				}
				System.out.println(sql);
				rows = stmt.executeUpdate();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		return rows;
	}

	public Integer delete(String campoWhere, String valorWhere) {
		String sql = "DELETE FROM " + this.tabela + " WHERE " + campoWhere + " = '" + valorWhere + "';";
		Integer rows = 0;

		System.out.println(sql);
		
		PreparedStatement stmt = null;

		if (this.conn.getConn() != null) {
			try {
				stmt = conn.getConn().prepareStatement(sql);
				rows = stmt.executeUpdate();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		return rows;
	}

	public String getTabela() {
		return tabela;
	}

	public void setTabela(String tabela) {
		this.tabela = tabela;
	}

	public String[] getCampos() {
		return campos;
	}

	public void setCampos(String[] campos) {
		this.campos = campos;
	}

	public DBConnection getConn() {
		return conn;
	}

	public void setConn(DBConnection conn) {
		this.conn = conn;
	}

}
