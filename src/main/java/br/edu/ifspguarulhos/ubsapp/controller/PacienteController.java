package br.edu.ifspguarulhos.ubsapp.controller;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.edu.ifspguarulhos.ubsapp.model.DBConnection;
import br.edu.ifspguarulhos.ubsapp.model.DBQuery;
import br.edu.ifspguarulhos.ubsapp.model.Paciente;

@RestController
@RequestMapping(value = "/api/paciente")
public class PacienteController {

	@GetMapping("/listarPacientes")
	public List<Paciente> listarPacientes() {
		Paciente umPaciente = null;
		List<Paciente> listaPacientes = new ArrayList<Paciente>();
		DBConnection conn = new DBConnection();
		DBQuery query = new DBQuery("Paciente", new String[] { "*" }, conn);
		ResultSet rs = query.select(null, null);

		try {
			while (rs.next()) {
				umPaciente = new Paciente();
				umPaciente.setNum_sus(rs.getLong(1));
				umPaciente.setNome(rs.getString(2));
				umPaciente.setRg(rs.getString(3));
				umPaciente.setCpf(rs.getString(4));
				umPaciente.setDataNascimento(rs.getDate(5));
				umPaciente.setEndereco(rs.getString(6));

				listaPacientes.add(umPaciente);
				umPaciente = null;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return listaPacientes;

	}

	@GetMapping("/pesquisarPaciente")
	public List<Paciente> pesquisarPaciente(@RequestBody Paciente pac) {
		Paciente paciente = null;
		List<Paciente> listaPacientes = new ArrayList<Paciente>();
		DBConnection conn = new DBConnection();
		DBQuery query = new DBQuery("Paciente",
				new String[] { "num_sus", "nome", "rg", "cpf", "data_nasc", "endereco" }, conn);

		ResultSet rs = query.select("nome", new String[] {"%" + pac.getNome() + "%"});

		try {
			while (rs.next()) {
				paciente = new Paciente();
				paciente.setNum_sus(rs.getLong(1));
				paciente.setNome(rs.getString(2));
				paciente.setRg(rs.getString(3));
				paciente.setCpf(rs.getString(4));
				paciente.setDataNascimento(rs.getDate(5));
				paciente.setEndereco(rs.getString(6));
				listaPacientes.add(paciente);
				paciente = null;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return listaPacientes;
	}

	@PostMapping("/cadastrarPaciente")
	public Boolean cadastrarPaciente(@RequestBody Paciente paciente) {
		Boolean resp = false;
		DBConnection conn = new DBConnection();
		DBQuery query = new DBQuery("Paciente",
				new String[] { "num_sus", "nome", "rg", "cpf", "data_nasc", "endereco" }, conn);
		Integer rows = query.insert(new Object[] { paciente.getNum_sus(), paciente.getNome(), paciente.getRg(),
				paciente.getCpf(), paciente.getDataNascimento(), paciente.getEndereco() });

		if (rows > 0) {
			resp = true;
		}

		return resp;
	}

	@PutMapping("/atualizarPaciente")
	public Boolean atualizarPaciente(@RequestBody Paciente paciente) {
		Boolean resp = false;
		DBConnection conn = new DBConnection();
		DBQuery query = new DBQuery("Paciente",
				new String[] { "num_sus", "nome", "rg", "cpf", "data_nasc", "endereco" }, conn);
		Integer rows = query.update(
				new Object[] { paciente.getNum_sus(), paciente.getNome(), paciente.getRg(), paciente.getCpf(),
						paciente.getDataNascimento(), paciente.getEndereco() },
				"num_sus", Long.toString(paciente.getNum_sus()));
		
		if (rows > 0) {
			resp = true;
		}
		
		return resp;
	}
	
	@DeleteMapping("/deletarPaciente")
	public Boolean deletarPaciente(@RequestBody Paciente paciente) {
		Boolean resp = false;
		DBConnection conn = new DBConnection();
		DBQuery query = new DBQuery("Paciente", new String[] {"num_sus"}, conn);
		Integer rows = query.delete("num_sus", Long.toString(paciente.getNum_sus()));
		
		if (rows > 0) {
			resp = true;
		}
		
		return resp;
	}
	
}
