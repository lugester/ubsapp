package br.edu.ifspguarulhos.ubsapp.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.edu.ifspguarulhos.ubsapp.model.DBConnection;
import br.edu.ifspguarulhos.ubsapp.model.Unidade;
import br.edu.ifspguarulhos.ubsapp.services.UnidadeService;


@CrossOrigin(origins = {"*"}, allowedHeaders = {"*"}, value = {"*"})
@RequestMapping(value="/api/ubs")
@RestController
public class UbsController {
	@Autowired
	UnidadeService unidadeService;

	@GetMapping("/listar")
	public List<Unidade> listarUbs(){
		
		System.out.println("CHEGUEI AQUI NO");
		List<Unidade> listaUBS = new ArrayList<Unidade>();
		
		
		listaUBS = unidadeService.pesquisarUnidades();		

		return listaUBS;
		
	}
	
	@GetMapping("/pesquisar")
	public List<Unidade> pesquisarUbsPorNome(@RequestBody Unidade unidade){
		DBConnection conn = new DBConnection();
		List<Unidade> listaUbs = new ArrayList<Unidade>();
		listaUbs = unidadeService.pesquisarUnidadePorNome(unidade.getNome());
		
		return listaUbs;
	}
	
	
	@PostMapping(value ="/cadastrarOuAtualizar")
	public Unidade cadastrarUBS(@RequestBody Unidade unidade) {
		
		return unidadeService.cadastrarOAtualizarUnidade(unidade);
	}
	
	
	@PutMapping("/atualizar")
	public Unidade atualizarUbs(@RequestBody Unidade ubs) {
		return unidadeService.cadastrarOAtualizarUnidade(ubs);
	}
	
	@DeleteMapping("/deletar")
	public Boolean deletarUbs(@RequestBody Unidade ubs) {
		unidadeService.deletarUmaUnidade(ubs);
		if (unidadeService.pesquisarUmaUnidadePorId(ubs.getIdUnidade()) != null) {
			return true;
		} else {
			return false;
		}
	}	
}
