package br.edu.ifspguarulhos.ubsapp.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.edu.ifspguarulhos.ubsapp.model.Funcionario;
import br.edu.ifspguarulhos.ubsapp.services.FuncionarioService;

@RestController
@RequestMapping(value = "/api/funcionario")
public class FuncionarioController {

	@Autowired
	FuncionarioService funcionarioService;

	@GetMapping("/listar")
	public List<Funcionario> listarFuncionario() {
		List<Funcionario> listaFuncionario = new ArrayList<Funcionario>();
		listaFuncionario = funcionarioService.pesquisarFuncionarios();

		return listaFuncionario;
	}

	@GetMapping("/pesquisarPorCargo")
	public List<Funcionario> listarFuncionarioPorCargo(@RequestBody String cargo) {
		List<Funcionario> listaFuncionario = new ArrayList<Funcionario>();
		System.out.println("Recebido: " + cargo);
		listaFuncionario = funcionarioService.pesquisarFuncionarioPorCargo(cargo);

		return listaFuncionario;
	}

	@GetMapping("/pesquisarPorNome")
	public List<Funcionario> pesquisarFuncionario(@RequestBody String nome) {
		List<Funcionario> listaFuncionario = new ArrayList<Funcionario>();

		listaFuncionario = funcionarioService.pesquisarFuncionarioPorNome(nome);

		return listaFuncionario;

	}

	@GetMapping("/pesquisarPorEmail")
	public Funcionario pesquisarFuncionarioPorEmail(@RequestBody String email) {

		Funcionario func = funcionarioService.pesquisarFuncionarioPorEmail(email);
		return func;

	}

	@GetMapping("/pesquisarPorUnidade")
	public List<Funcionario> pesquisarPorUnidade(@RequestBody Funcionario func) {
		List<Funcionario> listaFuncionario = new ArrayList<Funcionario>();

		listaFuncionario = funcionarioService.pesquisarFuncionarioPorUnidade(func.getUnidade().getIdUnidade());

		return listaFuncionario;
	}

	@SuppressWarnings("unused")
	@PostMapping("/cadastrarFuncionario")
	public Boolean cadastrarFuncionario(@RequestBody Funcionario funcionario) {
		Boolean resp = false;

		return resp;
	}

	@PutMapping("/atualizarFuncionario")
	public Boolean atualizarFuncionario(@RequestBody Funcionario func) {
		Boolean resp = false;

		return resp;
	}

	@DeleteMapping("/deletarFuncionario")
	public Boolean deletarFuncionario(@RequestBody Funcionario funcionario) {
		Boolean resp = false;

		return resp;
	}

}
