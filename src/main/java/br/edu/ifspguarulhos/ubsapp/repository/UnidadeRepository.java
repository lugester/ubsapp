package br.edu.ifspguarulhos.ubsapp.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.edu.ifspguarulhos.ubsapp.model.Funcionario;
import br.edu.ifspguarulhos.ubsapp.model.Unidade;

@Repository
public interface UnidadeRepository extends JpaRepository<Unidade, Integer> {
	
	public List<Unidade> findByIdDir(Funcionario idDir);
	public List<Unidade> findByNome(String nome);
	public List<Unidade> findByEndereco(String endereco);
}
