package br.edu.ifspguarulhos.ubsapp.repository;

import java.sql.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.edu.ifspguarulhos.ubsapp.model.Funcionario;

@Repository
public interface FuncionarioRepository extends JpaRepository<Funcionario, Integer>{
	public List<Funcionario> findByUnidade(Integer unidade);
	public List<Funcionario> findByCpf(String cpf);
	public List<Funcionario> findByNome(String nome);
	public List<Funcionario> findByCodFunc(Integer codFunc);
	public List<Funcionario> findByDataNasc(Date dataNasc);
	public Optional<Funcionario> findByEmail(String email);
	public List<Funcionario> findByCargo(String cargo);
	
	
}
