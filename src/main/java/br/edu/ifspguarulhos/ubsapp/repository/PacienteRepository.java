package br.edu.ifspguarulhos.ubsapp.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.edu.ifspguarulhos.ubsapp.model.Paciente;

@Repository
public interface PacienteRepository extends JpaRepository<Paciente, Long> {
	public List<Paciente> findByNome(String nome);
	public List<Paciente> findByNumeroSus(Long numeroSus);
	public List<Paciente> findByCpf(String cpf);
	public List<Paciente> findByEmail(String email);
	public List<Paciente> findByRg(String rg);
	public List<Paciente> findByEndereco(String endereco);
	
}
