package br.edu.ifspguarulhos.ubsapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@EnableAutoConfiguration
@SpringBootApplication
public class UbsappApplication {

	public static void main(String[] args) {
		SpringApplication.run(UbsappApplication.class, args);
	}

}
