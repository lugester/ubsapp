package br.edu.ifspguarulhos.ubsapp.services;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.security.crypto.password.PasswordEncoder;
import br.edu.ifspguarulhos.ubsapp.model.Funcionario;
import br.edu.ifspguarulhos.ubsapp.repository.FuncionarioRepository;

@Service
public class JwtUserDetailsService implements UserDetailsService {
	
	@Autowired
	private FuncionarioService funcionarioService;
	
	@Autowired
	private PasswordEncoder bcryptEncoder;
	
	@Autowired
	private FuncionarioRepository funcionarioRepository;

	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		Funcionario funcionario = funcionarioService.pesquisarFuncionarioPorEmail(username);
		if (funcionario == null) {
			throw new UsernameNotFoundException("User not found with username: " + username);
		} else {
			return new User(funcionario.getEmail(), funcionario.getSenha(),
					new ArrayList<>());
		}
	}
	
	public Funcionario save(Funcionario funcionario) {
		funcionario.setSenha(bcryptEncoder.encode(funcionario.getSenha()));
		return funcionarioRepository.save(funcionario);
	}

}