package br.edu.ifspguarulhos.ubsapp.services;

import java.io.Serializable;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.edu.ifspguarulhos.ubsapp.model.Funcionario;
import br.edu.ifspguarulhos.ubsapp.repository.FuncionarioRepository;

@Service
public class FuncionarioService implements Serializable {
	
	private static final long serialVersionUID = 1L;
	@Autowired
	private FuncionarioRepository repository;
	
	public List<Funcionario> pesquisarFuncionarios(){
		return repository.findAll();
	}
	
	public Funcionario pesquisarFuncionarioPorCodFunc(Integer id){
		return Optional.ofNullable(repository.findById(id).get()).orElse(null);
	}
	
	public List<Funcionario> pesquisarFuncionarioPorNome(String nome) {
		return repository.findByNome(nome);
	}
	
	public List<Funcionario> pesquisarFuncionarioPorCpf(String cpf) {
		return repository.findByCpf(cpf);
	}
	
	public List<Funcionario> pesquisarFuncionarioPorUnidade(Integer unidade) {
		return repository.findByUnidade(unidade);
	}
	
	public Funcionario pesquisarFuncionarioPorEmail(String email) {
		return Optional.ofNullable(repository.findByEmail(email).get()).orElse(null);

	}
	
	public List<Funcionario> pesquisarFuncionarioPorCargo(String cargo) {
		return repository.findByCargo(cargo);
	}
	
	public Funcionario cadastrarOuAtualizarFuncionario(Funcionario funcionario) {
		Optional<Funcionario> verificaFuncionario = repository.findById(funcionario.getIdFuncionario());
		
		if(verificaFuncionario.isPresent()) {
			
		}
		
		return null;
	}
}
