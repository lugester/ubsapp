package br.edu.ifspguarulhos.ubsapp.services;

import java.io.Serializable;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.edu.ifspguarulhos.ubsapp.model.Funcionario;
import br.edu.ifspguarulhos.ubsapp.model.Unidade;
import br.edu.ifspguarulhos.ubsapp.repository.UnidadeRepository;



@Service
public class UnidadeService implements Serializable{
	
	private static final long serialVersionUID = 1L;
	@Autowired
	private UnidadeRepository repository;

	public List<Unidade> pesquisarUnidades() {
		return repository.findAll();
	}

	public Optional<Unidade> pesquisarUmaUnidadePorId(Integer id) {
		return repository.findById(id);
	}

	public List<Unidade> pesquisarUnidadesPorId(List<Integer> ids) {
		return repository.findAllById(ids);
	}
	
	public List<Unidade> pesquisarUnidadePorNome(String nome) {
		return repository.findByNome(nome);
	}
	
	public List<Unidade> pesquisarUnidadesPorEndereco(String endereco) {
		return repository.findByEndereco(endereco);
	}
	
	public List<Unidade> pesquisarUnidadePorIdDiretor(Funcionario idDir) {
		return repository.findByIdDir(idDir);
	}
	
	public Unidade cadastrarOAtualizarUnidade(Unidade entity) {
		Optional<Unidade> unidadePesquisa = repository.findById(entity.getIdUnidade());
		
		if (unidadePesquisa.isPresent()) {
			Unidade atualizar = unidadePesquisa.get();
			atualizar.setEndereco(entity.getEndereco());
			atualizar.setNome(entity.getNome());
			atualizar.setIdDiretor(entity.getIdDiretor());
		
			atualizar = repository.save(atualizar);
			
			return atualizar;
		} else {
			entity = repository.save(entity);
			return entity;
		}
	}
	
	public void deletarUmaUnidadePorId(Integer id) {
		repository.deleteById(id);
	}
	
	public void deletarUmaUnidade(Unidade unidade) {
		repository.delete(unidade);	
	}
	
	
}
