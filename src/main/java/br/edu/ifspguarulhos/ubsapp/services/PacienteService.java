package br.edu.ifspguarulhos.ubsapp.services;

import java.io.Serializable;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.edu.ifspguarulhos.ubsapp.model.Paciente;
import br.edu.ifspguarulhos.ubsapp.repository.PacienteRepository;

@Service
public class PacienteService implements Serializable {

	@Autowired
	PacienteRepository repository;
	
	private static final long serialVersionUID = 1L;
	
	
	public List<Paciente> pesquisarPacientes() {
		return repository.findAll();
	}
	
	public List<Paciente> pesquisarPacientePorNome(String nome) {
		return repository.findByNome(nome);
	}
	
	public Optional<Paciente> pesquisarPacientePorNumSus(Long id){
		return repository.findById(id);
	}
	
	public List<Paciente> pesquisarPacientePorCpf(String cpf) {
		return repository.findByCpf(cpf);
	}
	
	public List<Paciente> pesquisarPacientePorRg(String rg) {
		return repository.findByRg(rg);
	}
	
	public List<Paciente> pesquisarPacientePorEmail(String email) {
		return repository.findByEmail(email);
	}
	
	public List<Paciente> pesquisarPacientePorEndereco(String endereco) {
		return repository.findByEndereco(endereco);
	}
}
